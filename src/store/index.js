import Vue from 'vue'
import Vuex from 'vuex'
import '../slim/slim/slim.css';
import slimCropper from '../slim/slim/slim.vue';

Vue.use(Vuex);
Vue.component('slimCropper', slimCropper);

import jugador from './jugador'

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    jugador,
  }
})
