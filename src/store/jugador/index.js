import axiosJugadores from "@/services/jugadores/jugadores.js";
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default {
    state: {
        jugadores: new Array,
        cantidad: '',
    },
    mutations: {
        set_jugadores(state, payload){
            state.jugadores = payload
        },
        set_cantidad(state, payload){
            state.cantidad = payload
        }
    },
    getters: {
        jugadores: state => state.jugadores,
        cantidad: state => state.cantidad
    },
    actions: {
        async obtener_jugadores({commit}, id){
            const data = await axiosJugadores.listarJugadores(id);
            if(data.data) {
                commit('set_jugadores', data.data.jugadores);
                commit('set_cantidad', data.data.cantidad);
            }
        }
    }
}
