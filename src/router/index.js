import Vue from 'vue'
import VueRouter from 'vue-router'
//middleware
import auth from '../middleware/auth.js';

//axios 
import axiosEquipos from '../services/equipos/equipos.js'
import axiosAdmins from '../services/auth/auth.js'


// import loginView from '../views/login.vue'
import login from '../components/login.vue'
import carga from '../components/pantallaCarga.vue'
import equipos from '../views/Equipos/equipos.vue'
import crearEquipos from '../views/Equipos/crearEquipos.vue'
import editarEquipos from '../views/Equipos/editarEquipo.vue'
import equiposInfo from '../views/Equipos/equiposInfo.vue'
import admins from '../views/Admins/administradores.vue'
import crearAdmins from '../views/Admins/crearAdmins.vue'
import adminsInfo from '../views/Admins/adminsInfo.vue'
import editarAdmins from '../views/Admins/editarAdmins.vue'
import config from '../views/configuracion/configuraciones.vue'

Vue.use(VueRouter)
const equiposGuards = async (to, from, next) => {
    let existe;
    const {data} = await axiosEquipos.listarUnEquipo(to.params.id);
    if (data.equipo.length != 0) {
        existe = true;
    }else{
        existe = false;
    }
    if (existe == true) {
        next();
    }else{
        next({name: "equipos"});
    }
}

const adminsGuards = async (to, from, next) => {
    let existe;
    const {data} = await axiosAdmins.listarUnAdmin(to.params.id);
    if (data.successfull == true) {
        existe = true;
    }else{
        existe = false;
    }
    if (existe == true) {
        next();
    }else{
        next({name: "administradores"});
    }
}


const routes = [{
    path: '/',
    name: 'Home',
    component: login,
    
},
{
    path: '/carga',
    name: 'pCarga',
    component: carga,
    
},
{
    path: '/configuracion',
    name: 'configuraciones',
    component: config,
    meta: {
        middleware: auth,
    }
},
//USARIOS

{
    path: '/equipos',
    name: 'equipos',
    component: equipos,
    meta: {
        middleware: auth,
    }
},
{
    path: '/equipos/:id', 
    name: 'equipos-info',
    component: equiposInfo,
    beforeEnter: equiposGuards,
    meta: {
        middleware: auth,
    }
},
{
    path: '/crear-equipos',
    name: 'crear-equipos',
    component: crearEquipos,
    meta: {
        middleware: auth,
    }
},
{
    path: '/editar-equipos/:id', 
    name: 'editar-equipos',
    beforeEnter: equiposGuards,
    component: editarEquipos,
    meta: {
        middleware: auth,
    }
},
// //ADMINISTRADORES

{
    path: '/administradores', 
    name: 'administradores',
    component: admins,
    meta: {
        middleware: auth,
    }
},
{
    path: '/administradores/:id', // recibe un id
    name: 'admins-info',
    component: adminsInfo,
    beforeEnter: adminsGuards,
    meta: {
        middleware: auth,
    }
},
{
    path: '/crear-administradores',
    name: 'crear-admins',
    component: crearAdmins,
    meta: {
        middleware: auth,
    }
},
{
    path: '/editar-administradores/:id', //recibe un id
    name: 'editar-admins',
    beforeEnter: adminsGuards,
    component: editarAdmins,
    meta: {
        middleware: auth,
    }
},
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
})

//Navigation Guards
//router.beforeEach


/*  */
// Creates a `nextMiddleware()` function which not only
// runs the default `next()` callback but also triggers
// the subsequent Middleware function.
function nextFactory(context, middleware, index) {
    const subsequentMiddleware = middleware[index];
    // If no subsequent Middleware exists,
    // the default `next()` callback is returned.
    if (!subsequentMiddleware) return context.next;

    return (...parameters) => {
        // Run the default Vue Router `next()` callback first.
        context.next(...parameters);
        // Then run the subsequent Middleware with a new
        // `nextMiddleware()` callback.
        const nextMiddleware = nextFactory(context, middleware, index + 1);
        subsequentMiddleware({ ...context, next: nextMiddleware });
    };
}

router.beforeEach((to, from, next) => {
    if (to.meta.middleware) {
        const middleware = Array.isArray(to.meta.middleware)
            ? to.meta.middleware
            : [to.meta.middleware];

        const context = {
            from,
            next,
            router,
            to,
        };
        const nextMiddleware = nextFactory(context, middleware, 1);

        return middleware[0]({ ...context, next: nextMiddleware });
    }

    return next();
});

export default router