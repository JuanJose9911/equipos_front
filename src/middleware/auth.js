export default function auth({ next, router }) {
    if (!localStorage.getItem('accesToken')) {
      return router.push({ name: 'Home' });
    }
  
    return next();
  }