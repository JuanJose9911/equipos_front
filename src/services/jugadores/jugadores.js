import axios from '../../api/axios.js';

export default {

    registrarJugador(form){
        return axios.post(`registrar-jugador`, form);
    },

    listarJugadores(id){
        return axios.get(`jugadores/${id}`);
    },
    editarJugador(id, form){
        return axios.put(`editar-jugador/${id}`, form)
    },
    borrarJugador(id){
        return axios.delete(`eliminar-jugador/${id}`)
    }
}