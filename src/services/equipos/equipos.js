import axios from '../../api/axios.js';

export default {

    crearEquipo(form){
        return axios.post(`crear-equipo`, form);
    },

    listarEquipos(){
        return axios.get('equipos');
    },

    listarUnEquipo(id){
        return axios.get(`equipos/${id}`)
    },
    editarEquipo(id, form){
        return axios.put(`editar-equipo/${id}`, form)
    },

    listarPaises(){
        return axios.get(`listar-paises`);
    },

    listarCiudades(id){
        return axios.get(`listar-ciudades/${id}`);
    },

    borrarEquipo(id){
        return axios.delete(`eliminar-equipos/${id}`);
    },
    editarImagen(id, form){
        return axios.put(`editar-imagen/${id}`, form);
    },
    editarImagenCoach(id, form){
        return axios.put(`editar-imagen-coach/${id}`, form);
    }
}