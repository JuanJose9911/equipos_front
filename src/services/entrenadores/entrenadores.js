import axios from '../../api/axios.js';

export default {

    crearEntrenador(form){
        return axios.post(`crear-entrenador`, form);
    },

    listarEntrenador(id){
        return axios.get(`entrenador/${id}`);
    },
    editarEntrenador(id, form){
        return axios.put(`editar-entrenador/${id}`, form)
    },
}