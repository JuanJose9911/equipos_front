import axios from "../../api/axios.js";


// };

export default {
    register(form) {
        return axios.post(`register`, form);
    },
    me(){
        return axios.post(`me`);
    },
    listarAdmins(){
        return axios.get('administradores');
    },
    listarUnAdmin(id){
        return axios.get(`administradores/${id}`);
    },
    editarAdmin(id, form){
        return axios.put(`editar-administradores/${id}`, form);
    },
    borrarAdmin(id){
        return axios.delete(`borrar-administradores/${id}`);
    },
    login(form) {
        return axios.post(`login`, form);
    },
    logout(){
        return axios.post('logout');
    },
    editarImagen(id, form){
        return axios.put(`editar-imagen/${id}`, form);
    }
};
