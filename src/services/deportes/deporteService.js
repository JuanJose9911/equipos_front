import axios from "../../api/axios.js";


export default {

    crearDeporte(form){
        return axios.post(`registrar-deporte`, form);
    },

    listarDeportes(){
        return axios.get('deportes');
    },

    editarDeporte(id, form){
        return axios.put(`editar-deporte/${id}`, form)
    },

    eliminarDeporte(id){
        return axios.delete(`eliminar-deporte/${id}`);
    }
};