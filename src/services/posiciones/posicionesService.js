import axios from "../../api/axios.js";


export default {

    crearPosicion(form){
        return axios.post(`crear-posicion`, form);
    },

    listarPosiciones(id){
        return axios.get(`listar-posiciones/${id}`);
    },

    editarPosicion(id, form){
        return axios.put(`editar-posicion/${id}`, form);
    },

    eliminarPosicion(id){
        return axios.delete(`eliminar-posicion/${id}`);
    }
};